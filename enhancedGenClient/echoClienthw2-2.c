/*
				Refferences	
	- The following are the links from which parts of the program below have been used from.
		- https://www.geeksforgeeks.org/socket-programming-cc/
		- https://stackoverflow.com/questions/4181784/how-to-set-socket-timeout-in-c-when-making-multiple-connections
		- https://beej.us/guide/bgnet/html/multi/inet_ntopman.html
		- https://beej.us/guide/bgnet/html/multi/gethostbynameman.html
		- https://cboard.cprogramming.com/linux-programming/93565-getting-service-port-getservbyname.html
*/

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#define PORT 7
int main(int argc, char *argv[]){
    printf("Number of arguments: %i\n", argc);
    
   	
   	if(argc < 5){
   		fprintf(stderr,"Incorrect number of arguments, must 4 or more, with the following format:\n\t");
   		fprintf(stderr,"echoclient MyString x.x.x.x -udp\n");
   		return -1;
   	}
		
	char* hello;
	char* host_name;
	char* service;
	int spaceNeededForString = 0;

	if(argc == 5){
		hello = argv[1];
		spaceNeededForString = strlen(argv[1]);
	   	host_name = argv[2];
	   	service = argv[3];
   	}
   	else{
		fprintf(stderr, "\n Error: Arguments passed must be in the following format. \n\t");
		fprintf(stderr, "echoclient \"MyString\" ????.com service_name -tcp\n\t");
	}
	
   	char recvBuff[spaceNeededForString + 1];
   	memset(recvBuff, '\0', sizeof(recvBuff)); 
//	recvBuff[spaceNeededForString] = '\0';
	
	//AF_INET-2 and AF_INET6-10
	printf("Host name: %s\n", host_name);
	struct hostent* tmp = gethostbyname(host_name);
	struct in_addr **addr_list;
	printf("%s\n", "Here i m");
	addr_list = (struct in_addr **)tmp->h_addr_list;
	char* ip_addr = inet_ntoa(*addr_list[0]);
	printf("IP address: %s\n ", ip_addr);
	int addr_type = tmp->h_addrtype;
 	
	
	//TCP
    if(strncmp(argv[argc-1], "-tcp", 4) == 0){
    
    	//Port number
		struct servent* serv = getservbyname(service, "tcp");
		unsigned int port = ntohs(serv->s_port);
		printf("Port is: %i\n", port);
    
		int sockfd = socket(addr_type, SOCK_STREAM, 0);
		
		if(sockfd < 0){
		    fprintf(stderr, "\n Error: socket not created.\n");
		    return 1;
		}
		
		struct timeval timeout;
		timeout.tv_sec = 3;
		timeout.tv_usec = 0;
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		
		//serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
		
		
		if(addr_type == AF_INET){
			struct sockaddr_in serv_addr;
			
			serv_addr.sin_family = addr_type;
			serv_addr.sin_port = htons(port);
			
			if(inet_pton(addr_type, ip_addr, &serv_addr.sin_addr) <= 0){	
				fprintf(stderr, "\n Error: Invalid address.\n"); 
				return -1;
			}
			
			if(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
				fprintf(stderr, "\n Error: Could not establish connection, timeout.\n");
				return -1;
			}
		
		}
		if(addr_type == AF_INET6){
			struct sockaddr_in6 serv_addr;
		
			serv_addr.sin6_family = addr_type;
			serv_addr.sin6_port = htons(port);
			
			if(inet_pton(addr_type, ip_addr, &serv_addr.sin6_addr) <= 0){	
				fprintf(stderr, "\n Error: Invalid address.\n"); 
				return -1;
			}
			
			if(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
				fprintf(stderr, "\n Error: Could not establish connection, timeout.\n");
				return -1;
			}	
		}
		
		
		send(sockfd, hello, strlen(hello), 0);
		
		int valread = 0;
		long valread2;
		
		if(port == 37) //time
		{
			valread = read(sockfd, (char*)&valread2, sizeof(valread2));
			fprintf(stdout, "\This time in epoch time format: %ld\n", ntohl(valread2));
		}
		else if(port == 9) // discard
		{
			fprintf(stdout, "\n Since the service is discard, not expecting anything to return.\n");
		}
		else
		{
			valread = read(sockfd, recvBuff, sizeof(recvBuff));
			fprintf(stdout, "%s\n", recvBuff);
		}
		
		
		
		close(sockfd);
    }
    //UDP
    else if(strncmp(argv[argc-1], "-udp", 4) == 0){
    	struct servent* serv = getservbyname(service, "udp");
		unsigned int port = ntohs(serv->s_port);
		printf("Port is: %i\n", port);
    	
    	int sockfd = socket(addr_type, SOCK_DGRAM, 0);
		
		if(sockfd < 0){
		    fprintf(stderr, "\n Error: socket not created.\n");
		    return 1;
		}
		
		struct timeval timeout;
		timeout.tv_sec = 3;
		timeout.tv_usec = 0;
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		//give the ip address 184.72.104.138 duckduckgo, and port number 7
		struct sockaddr_in serv_addr;
		
		serv_addr.sin_family = addr_type;
		serv_addr.sin_port = htons(port);
		//serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
		
		
		if(addr_type == AF_INET){
			struct sockaddr_in serv_addr;
			
			serv_addr.sin_family = addr_type;
			serv_addr.sin_port = htons(port);
			
			if(inet_pton(addr_type, ip_addr, &serv_addr.sin_addr) <= 0){	
				fprintf(stderr, "\n Error: Invalid address.\n"); 
				return -1;
			}
			
			sendto(sockfd, hello, strlen(hello), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
		}
		if(addr_type == AF_INET6){
			struct sockaddr_in6 serv_addr;
		
			serv_addr.sin6_family = addr_type;
			serv_addr.sin6_port = htons(port);
			
			if(inet_pton(addr_type, ip_addr, &serv_addr.sin6_addr) <= 0){	
				fprintf(stderr, "\n Error: Invalid address.\n"); 
				return -1;
			}
			
			sendto(sockfd, hello, strlen(hello), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
		}
		
		struct sockaddr from;
		socklen_t len = sizeof(from);
		
		int valread = 0;
		long valread2;
		
		if(port == 37) //time
		{
			valread = recvfrom(sockfd, (char*)&valread2, sizeof(valread2), 0, NULL, NULL);
			fprintf(stdout, "\This time in epoch time format: %ld\n", ntohl(valread2));
		}
		else if(port == 9) //discard
		{
			fprintf(stdout, "\n Since the service is discard, not expecting anything to return.\n");
		}
		else
		{
			valread = recvfrom(sockfd, recvBuff, sizeof(recvBuff), 0, NULL, NULL);
			fprintf(stdout, "%s\n", recvBuff);
		}
			
		
		close(sockfd);
    }
    else{
    	fprintf(stderr, "\n Error: Please choose one of the following options: \n\t -tcp \t -udp \n");
    }
    
    return 0;
}
