/**
 * Referenc:
 * 	- https://www.cs.cmu.edu/afs/cs/academic/class/15213-f00/unpv12e/ping/
 *	- https://www.cs.utah.edu/~swalton/listings/sockets/programs/part4/chap18/myping.c
 		- https://www.cs.utah.edu/~swalton/listings/sockets/programs/part4/chap18/myping.c*/

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>


int sockfd = 0;
const int BUFFSIZE = 65000;
char sendBuf[65000] = {'0'};
char recvBuf[65000] = {'0'};
struct sockaddr_in serv_addr, server;

void parseReturnValue(int);
int passedParametersAreOk(int, char*[]);
int ipAddrOk(char* argv[]);
void setUpTheICMPInSendBuff(int, char *argv[]);

/**The main*/
int main(int argc, char* argv[]){

	if(passedParametersAreOk(argc, argv) != 0){
		fprintf(stderr, "Error: something wrong with the paramters passed in.\n");
		return -1;
	}

	//create socket
	if((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0){
		fprintf(stderr, "Error: socket gave an error: %s \n", strerror(errno));
		return -1;
	}

	int index = 0;
	while(1){
		setUpTheICMPInSendBuff(index, argv);

		socklen_t len = sizeof(serv_addr);
		if(sendto(sockfd, sendBuf, BUFFSIZE, 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
			fprintf(stderr, "Error: couldn't send: %s\n", strerror(errno));
			return -1;
		}

		socklen_t len2 = sizeof(server);
		int bytes = 0;
		if(bytes = recvfrom(sockfd, recvBuf, sizeof(recvBuf), 0, (struct sockaddr *)&server, &len2) < 0){
			fprintf(stderr, "Error: Receiving gave an error: %s \n", strerror(errno));
			return -1;
		}

		char parsedStr[BUFFSIZE];
		parseReturnValue(bytes);
		//fprintf(stdout, "This is recieved: %s \n", recvBuf);
		index++;
		sleep(1);
	}
	close(sockfd);
	return 0;
}

/**Check if the ip address is legit*/
int ipAddrOk(char* argv[]){
	serv_addr.sin_family = AF_INET;
	//serv_addr->sin_port = htons(port);
	char *ip = argv[2];

	if(inet_pton(AF_INET, ip, &(serv_addr.sin_addr)) <= 0){
		fprintf(stdout, "The address given isn't an IP address.\n");
		struct hostent* tmp = gethostbyname(ip);
		struct in_addr **addr_list;
		addr_list = (struct in_addr **)tmp->h_addr_list;
		char* ip_addr = inet_ntoa(*addr_list[0]);
		fprintf(stdout, "This is the ip address of the given domain name: %s\n", ip_addr);

		if(inet_pton(AF_INET, ip_addr, &(serv_addr.sin_addr)) <= 0){
			fprintf(stderr, "\n Error: Invalid address.\n");
			return -1;
		}
		else
			return 0;
	}
	else{
		return 0;
	}

	return -1;
}

/**Parse the returned string*/
void parseReturnValue(int bytes){
	int i;
	struct iphdr *ip = (void*)recvBuf;
	struct icmp*icmp = (void*)recvBuf+ip->ihl*4;

	printf("----------------\n");
	printf("\n");
	printf("IPv%d: hdr-size=%d pkt-size=%d protocol=%d TTL=%d",
		ip->version, ip->ihl*4, ntohs(ip->tot_len), ip->protocol,
		ip->ttl);

//IPv4: hdr-size=20 pkt-size=60 protocol=1 TTL=64ICMP: type[8/0] checksum[57171] id[0] seq[0]

	if ( icmp->icmp_id == 0 ){
			printf("ICMP: type[%d/%d] checksum[%d] sequence[%d] data[%s]\n",
			icmp->icmp_type, icmp->icmp_code, ntohs(icmp->icmp_cksum), icmp->icmp_seq,
			icmp->icmp_data);
	}
}

/**Check if the given parameters are ok*/
int passedParametersAreOk(int argc, char* argv[]){
	if(argc != 7)
		return -1;
	else if(strcmp(argv[1], "-ip") != 0)
		return -1;
	else if(strcmp(argv[3], "-sz") != 0)
		return -1;
	else if(strcmp(argv[5], "-string") != 0)
		return -1;

	int passed_len = 0;
	if( (passed_len = (int) atoi(argv[4])) < 0){
		fprintf(stderr, "Coudn't convert given string size to int: %s \n", strerror(errno));
		return -1;
	}

	if(strlen(argv[6]) != passed_len){
		fprintf(stderr, "Passed string len doesn't match the actual string len.\n");
		return -1;
	}


	if(ipAddrOk(argv) != 0){
		fprintf(stderr, "Error: Ip you gave didnt work.\n");
		return -1;
	}

	return 0;
}

/* Check sum function*/
unsigned short checksum(void *b, int len){
	unsigned short *buf = b;
	unsigned int sum=0;
	unsigned short result;

	for ( sum = 0; len > 1; len -= 2 )
		sum += *buf++;
	if ( len == 1 )
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return result;
}

/**Set up the char array as the icmp packet*/
void setUpTheICMPInSendBuff(int index, char* argv[]){
	int			len;
	struct icmp	*icmp;

	icmp = (struct icmp *) sendBuf;
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_code = 0;
	//icmp->icmp_id = 1;
	icmp->icmp_seq = index;
	//Gettimeofday((struct timeval *) icmp->icmp_data, NULL);

	//len = 8 + 10;		/* checksum ICMP header and data */
	icmp->icmp_cksum = 0;

	memcpy(icmp->icmp_data, argv[6], strlen(argv[6])+1);
	icmp->icmp_cksum = checksum(&icmp, sizeof(icmp));
}
