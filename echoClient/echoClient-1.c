/*
				Refferences	
	- The following are the links from which parts of the program below have been used from.
		- https://www.geeksforgeeks.org/socket-programming-cc/
		- https://stackoverflow.com/questions/4181784/how-to-set-socket-timeout-in-c-when-making-multiple-connections
*/

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#define PORT 7
int main(int argc, char *argv[]){
    printf("Number of arguments: %i\n", argc);
    
   	
   	if(argc < 4){
   		fprintf(stderr,"Incorrect number of arguments, must 4 or more, with the following format:\n\t");
   		fprintf(stderr,"echoclient MyString x.x.x.x -udp\n");
   		return -1;
   	}
		
	char* hello;
	char* ip_addr;
	int spaceNeededForString = 0;

	if(argc == 4){
		hello = argv[1];
		spaceNeededForString = strlen(argv[1]);
	   	ip_addr = argv[2];
   	}
   	else{
		fprintf(stderr, "\n Error: Arguments passed must be in the following format. \n\t");
		fprintf(stderr, "echoclient \"MyString\" 10.10.10.8 -tcp \n\t");
	}
	
   	//~ else{
   		//~ //if argument passed are more than 4, that means the string is divided by spaces.
   		//~ //	so the number or arguments passed which are part of the string are:- #totalOfArg - 3
   		//~ int numOfStringArg = argc - 3;
   
   		//~ for(int i = 1; i <= numOfStringArg; i++){
   			//~ printf("%s,", argv[i]);
   			//~ spaceNeededForString = spaceNeededForString + strlen(argv[i]) + 1; //+1 for spaces
   		//~ }
   		//~ printf("\n");
   		
   		//~ char temp[spaceNeededForString];
   		
   		//~ //To combine the different args to one string
   		//~ strcpy(temp, argv[1]);
   		//~ for(int i = 2; i <= numOfStringArg; i++){
   			//~ if(i != numOfStringArg - 3);
   				//~ strcat(temp, " ");
   			//~ strcat(temp, argv[i]);
   		//~ }
   		
   		//~ hello = temp;
   		//~ ip_addr = argv[argc-2];
   	//~ }
   	char recvBuff[spaceNeededForString + 1];
   	memset(recvBuff, '\0', sizeof(recvBuff)); 
//	recvBuff[spaceNeededForString] = '\0';
	
	//TCP
    if(strncmp(argv[argc-1], "-tcp", 4) == 0){
    	
		int sockfd = socket(AF_INET, SOCK_STREAM, 0);
		
		if(sockfd < 0){
		    fprintf(stderr, "\n Error: socket not created.\n");
		    return 1;
		}
		
		struct timeval timeout;
		timeout.tv_sec = 3;
		timeout.tv_usec = 0;
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		struct sockaddr_in serv_addr;
		
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(PORT);
		serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
		
		int conversion_ok = inet_pton(AF_INET, ip_addr, &serv_addr.sin_addr);
		
		if(conversion_ok <= 0){	
			fprintf(stderr, "\n Error: Invalid address.\n"); 
			return -1;
		}
		
		if(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
			fprintf(stderr, "\n Error: Could not establish connection, timeout.\n");
			return -1;
		}
		
		send(sockfd, hello, strlen(hello), 0);
		
		int valread = read(sockfd, recvBuff, sizeof(recvBuff));
		
		fprintf(stdout, "%s\n", recvBuff);
		
		close(sockfd);
    }
    //UDP
    else if(strncmp(argv[argc-1], "-udp", 4) == 0){
    	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
		
		if(sockfd < 0){
		    fprintf(stderr, "\n Error: socket not created.\n");
		    return 1;
		}
		
		struct timeval timeout;
		timeout.tv_sec = 3;
		timeout.tv_usec = 0;
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
			fprintf(stderr, "Error: setsockopt failed\n");
		
		//give the ip address 184.72.104.138 duckduckgo, and port number 7
		struct sockaddr_in serv_addr;
		
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(PORT);
		serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
		
		int conversion_ok = inet_pton(AF_INET, ip_addr, &serv_addr.sin_addr);
		
		if(conversion_ok <= 0){	
			fprintf(stderr, "\n Error: Invalid address.\n"); 
			return -1;
		}
		
		sendto(sockfd, hello, strlen(hello), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
		
		
		struct sockaddr from;
		socklen_t len = sizeof(from);
		int valread = recvfrom(sockfd, recvBuff, sizeof(recvBuff), 0, NULL, NULL);
			
		fprintf(stdout, "%s\n", recvBuff);
		
		close(sockfd);
    }
    else{
    	fprintf(stderr, "\n Error: Please choose one of the following options: \n\t -tcp \t -udp \n");
    }
    
    return 0;
}
