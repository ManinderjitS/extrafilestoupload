/***
 * References
 * 	- https://stackoverflow.com/questions/314401/how-to-read-a-line-from-the-console-in-c
 * 	- https://stackoverflow.com/questions/4426016/sending-a-struct-with-char-pointer-inside
 *	- https://stackoverflow.com/questions/2141277/how-to-zero-out-new-memory-after-realloc#2141327**/

//There are threads involved so need -pthread

//"bye" is to exit

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

//global variables
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
const int MAX_BUFFER_SIZE = 65536;
char opcode = ' ';

/***This struct is used to make some tasks easier**/
struct arg_struct{
	bool active;
	int port;
	char ip_domain_name[120]; 
};

/**This is the message struct**/
struct mssg_struct{
	unsigned short mssg_len;
	unsigned char opcode;
	char text[1];
};

//enum for char array indexes
//The package sent over the socket will be a char array with the following format
//	['mssg_len','mssg_len','mssg_len','mssg_len','mssg_len','Opcode','txt','txt','txt','txt', ...]
enum package_index {PackageLen = 0, Opcode = 6, Text = 8};

/***Function prototypes*/
bool clientRole(struct arg_struct*);
struct mssg_struct* convertMssgArrayToStruct(const char*);
char* convertMssgStructToArray(struct mssg_struct*, char*);
struct mssg_struct* createPacketFromData(const char*, size_t, const char);
size_t getUserInput(char**);
bool passedParametersAreOk(int, char**, struct arg_struct*);
struct mssg_struct* receiveStructArray(int sockfd);
bool serverRole(struct arg_struct*);
bool setSocketTimeOut(int); 
bool setUpServerInfo(struct sockaddr_in*, struct arg_struct*);
void startChatting(int, char* );
void startInteracting(int, bool);
void* sendChatMessages(void*);
void sendStructAsCharArr(int sockfd, struct mssg_struct*);


/****Main method*/
int main(int argc, char* argv[]){
	struct arg_struct args;
	
	//First check for number of arguements	
	if(!passedParametersAreOk(argc, argv, &args)){
		fprintf(stderr, "The number of parameters passed were more or less than the actual amount, which is 6.\n");
		return -1;
	}
	
	//Now setup the connection (server or client)
	//Passive = server
	//Active = client
	if(args.active){//This app plays the client role
		if(!clientRole(&args)){
			fprintf(stderr, "\n Error: Couldn't set up the client.\n");
			return -1;
		}
	}
	else{//This app plays the server role
		if(!serverRole(&args)){
			fprintf(stderr, "\n Error: Couldn't set up the server.\n");
			return -1;
		}
	}
	
	return 0;
}

/**This method is called when the application needs
 *  to play the role of a client.*/
bool clientRole(struct arg_struct* args){
	//Assuming only IPv4 connections
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	//Setting up the timeout
	setSocketTimeOut(sockfd);
	
	//Set up the server information 
	struct sockaddr_in serv_addr;
	if(!setUpServerInfo(&serv_addr, args)){
		fprintf(stderr, "\n Error: The address given wasn't a valid address.\n");
		return false;
	}
	
	//Try to connect
	if(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		fprintf(stderr, "\n Error: Could not establish connection, timeout.\n");
		return false;
	}
	
	//Start interacting
	startInteracting(sockfd, args->active);
		
	return true; 
} 


/**This method is called when the application needs 
 * to play the role of a server*/
bool serverRole(struct arg_struct* args){
	//Make a socket
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	struct sockaddr_in addrport;
	
	addrport.sin_family = AF_INET;
	addrport.sin_port = htons(args->port);
	addrport.sin_addr.s_addr = INADDR_ANY;
	
	//bind
	if(bind(sockfd, (struct sockaddr *) &addrport, sizeof(addrport)) < 0){
		fprintf(stderr, "\nError: couldn't bind to the socket.\n");
		return false;
	}
	
	//listen
	if(listen(sockfd, 5) < 0){//Hardcoded to 5 
		fprintf(stderr, "\nError: couldn't listen.\n ");
		return false;
	}
	
	int addrportLen = sizeof(addrport);
	
	fprintf(stdout, "Waiting for connections ...\n");
			
	//accept 
	while(1){
		int slaveSock = 0;
		if((slaveSock = accept(sockfd, (struct sockaddr *)&addrport, (socklen_t*)&addrportLen)) <= 0){
			fprintf(stderr,"\nError: couldn't accept.\n");
		}
		else{
			//Set the socket timeout for the slave socket
			//setSocketTimeOut(slaveSock);
			
			//Start the chatting part 
			startInteracting(slaveSock, args->active);	
		}
	}
	
	//	
	return true;
}

/***This method does all the chatting and stuff with the client/server*/
void startInteracting(int sockfd, bool active){
	char *buff = NULL;
	struct mssg_struct* mssg;
	char* name = NULL;
	char op = ' ';
	
	//fprintf(stdout, "Here in start interacting.\n");
	
	if(active){//If this is the client
		
		//First we will send the name, get the user input for that
		fprintf(stdout, "Please tell the person on the other side your name: ");
		size_t txt_len;
		do{
			txt_len = getUserInput(&buff);
			if(txt_len == 0)
				fprintf(stderr, "\n Error: error getting all the input from the user, please try again. \n");
		}while(txt_len == 0);
		
		mssg = createPacketFromData(buff, txt_len, opcode);
		mssg->opcode = '1';
		//Send the packet (this will be the name)
		fprintf(stderr, "Sending the name :> %s\n", mssg->text, txt_len);
		sendStructAsCharArr(sockfd, mssg);
			
		free(mssg);
		free(buff);
		
		//Wait for the respond, (the other side's name)
		mssg = receiveStructArray(sockfd);
		
		op = mssg->opcode;
		
		pthread_mutex_trylock(&mutex);
		opcode = op;
		pthread_mutex_unlock(&mutex);
		
		
		if(opcode == '1'){
			size_t name_len = strlen(mssg->text);
			name = malloc(name_len);
			memcpy(name, mssg->text, name_len);
			fprintf(stdout, "Name of the person you are talking to is %s\n", name);
		}
		
		free(mssg);
		
		startChatting(sockfd, name);
	}
	else{//If this is the server
		fprintf(stdout, "Someone has connected, waiting for their name ...\n");
		
		//First recieve will be the name		
		mssg = receiveStructArray(sockfd);
		
		op = mssg->opcode;
		
		pthread_mutex_trylock(&mutex);
		opcode = op;
		pthread_mutex_unlock(&mutex);
		
		if(opcode == '1'){
			size_t name_len = strlen(mssg->text);
			name = malloc(name_len);
			memcpy(name, mssg->text, name_len);
			fprintf(stdout, "Name of the person you are talking to is %s\n", name);
		}
		
		free(mssg);
		
		//Send your own name
		fprintf(stdout, "Please tell the person on the other side your name: ");
		size_t txt_len;
		do{
			txt_len = getUserInput(&buff);
			if(txt_len == 0)
				fprintf(stderr, "\n Error: error getting all the input from the user, please try again. \n");
		}while(txt_len == 0);
		
		size_t tmp2 = strlen(buff);
		
		mssg = createPacketFromData(buff, tmp2, opcode);
		mssg->opcode = '1';
		//Send the packet (this will be the name)
		sendStructAsCharArr(sockfd, mssg);
		
		free(mssg);
		free(buff);
		
		startChatting(sockfd, name);
	}
}

/***------These are all helper functions beyond this point----
 * meaning these functions help modify or setup data for other functions to use**/

/**This method converts the given char buffer to a mssg_struct
 * object, and returns a pointer to the allocated object*/
struct mssg_struct* convertMssgArrayToStruct(const char* buff){
	size_t txt_len = strlen(buff);
	
	//First set up the conversion variables
	int num_of_un_short_char = 5;
	char un_short[5] = {'0'};
	char opcode[1] = {'0'};
	char text[txt_len - 6];
	
	//Then get the values from the buffer into the variables above
	memcpy(un_short, buff, 5);//The unsigned short only takes up 5 characters
	
	//Then allocate memory for the struct
	struct mssg_struct* packet = (struct mssg_struct* ) malloc(txt_len);
	
	//Now start copying things over
	packet->mssg_len = (unsigned short) strtoul(un_short, NULL, 0);
	memcpy(&packet->opcode, buff + 5, 1);//The 2 is the position in the buffer string, opcode only takes 1 character
	memcpy(packet->text, buff + 6, txt_len - 6);//Rest is the text
	
	//fprintf(stdout, "This txt: %s \n", packet->text);
	
	return packet;
}

/**This function converts the struct mssg_struct to a char array, and
 * returns the length of the char array**/ 
char* convertMssgStructToArray(struct mssg_struct* mssg, char* arr){
	unsigned short text_len = mssg->mssg_len - 2; //Two because mssg_len and opcode are both 1 byte
	char text_arr[text_len];
	
	memcpy(text_arr, mssg->text, text_len);
	
	//Get the string value of the struct into a local char array
	unsigned short padding_len = 0;
	char padding[4] = {'\0'};
	
	for(int i = 10000; i > 1; i = i/10){
		if(mssg->mssg_len < i)
			padding_len++;
	}
	
	//make the padding string
	strcpy(padding, "0");
	for(int i = 1; i < padding_len; i++){
		strcat(padding, "0");
	}
	
	char mssg_array[mssg->mssg_len + padding_len + 1];
	
	sprintf(mssg_array, "%s%hu%c%s\0", padding, mssg->mssg_len, mssg->opcode, mssg->text);
	
	//Allocate memory so the local char array can be sent to another function 
	unsigned short mssg_len = strlen(mssg_array);
	char* final_struct_array = (char*) malloc(mssg_len);
	memcpy(final_struct_array, mssg_array, mssg_len);

	//arr = mssg_array;
	//fprintf(stdout, "This is the final char array to send %s. \n", arr);
	return final_struct_array;
}

/**Make a packet using the mssg_struct, which would
 * also include the data in the struct as well**/
struct mssg_struct* createPacketFromData(const char* buff, size_t buff_len, const char opcode){
	
	unsigned short len = sizeof(struct mssg_struct) + buff_len -1;
	struct mssg_struct* packet = (struct mssg_struct* ) malloc(len);
	
	packet->mssg_len = len;
	packet->opcode = opcode;
	memcpy(packet->text, buff, buff_len);
		
	return packet;
}

/**This function get the user input in the console*/
size_t getUserInput(char** buff){
	int ch; //as getchar() returns `int`

	char* line;
	
	if( (line = malloc(sizeof(char))) == NULL){ //allocating memory
        //checking if allocation was successful or not
        printf("unsuccessful allocation");
        return false;
    }

	int line_size = 0;
    line[0]='\0';
    size_t oldSize = 0;
    size_t newSize = 0;

    for(int index = 0; ( (ch = getchar())!='\n' ) && (ch != EOF) ; index++){
		oldSize = strlen(line);
		newSize = (index + 2)*sizeof(char);
		//The reallocate method is to resize the allocated memory
		
		
        if( (line = realloc(line, newSize)) == NULL){
			fprintf(stderr, "\n Error: reallocation error.\n");
			return 0;
		}
		if(newSize > oldSize){ //Clearing the new space 
			size_t diff = newSize - oldSize;
			void* pStart = ((char*) line) + oldSize;
			memset(pStart, '\0', diff);
		}
        
        line[index] = (char) ch; //type casting `int` to `char`
        line[index + 1] = '\0'; //inserting null character at the end 
		line_size = index;
    }
    
    line_size = line_size+2;
    
    line[line_size] = (char) '\0';
    size_t full_len = strlen(line);
    //Clear the memory at the end of the string
   
    *buff = line;
	return full_len;
}


/**This function checks whether the passed parameters 
 * are legit or are in the correct format**/
bool passedParametersAreOk(int argc, char* argv[], struct arg_struct* args){
	
	if(argc == 6 || argc == 4){
		for(int i = 0; i < argc; i++){
			if(strcmp(argv[i],"--active") == 0){
				if(argc == 6)
					args->active = true;
				else{
					fprintf(stderr, "\nError: Active must have 6 arguements passed in.\n");
					return false;
				}
			}
			else if(strcmp(argv[i],"--passive") == 0){
				if(argc == 4)
					args->active = false;
				else{
					fprintf(stderr, "\nError: Passive only needs 4 arguements.\n");
					return false;
				}
			}
			else if(strcmp(argv[i],"--port") == 0){
				i++;
				if(i >= argc)
					return false;
				args->port = atoi(argv[i]);
			}
			else if(strcmp(argv[i],"--peer") == 0){
				i++;
				if(i >= argc)
					return false;
				strcpy(args->ip_domain_name, argv[i]);
			}
				
		}
		return true;
	}
	return false;
}

/**This method recieves the char array version of the mssg_struct
 * and returns an allocated mssg_struct**/
struct mssg_struct* receiveStructArray(int sockfd){
	char recvBuff[65536] = {'\0'};
	
	pthread_mutex_trylock(&mutex);
	int count = recv(sockfd, (char*)&recvBuff, MAX_BUFFER_SIZE, 0);
	pthread_mutex_unlock(&mutex);
	
	//Get the mssg_struct from this array of chars
	struct mssg_struct* mssg = convertMssgArrayToStruct(recvBuff);
	
	return mssg;
}

/**This function is supposed to be used by a thread
 * to always waiting for user input*/
void* sendChatMessages(void* s){
	int* sockfd = (int*)s;
	char* buff = NULL;
	struct mssg_struct* mssg;
	size_t txt_len;
	
	while(opcode != '3'){
		do{
			txt_len = getUserInput(&buff);
			if(txt_len == 0)
				fprintf(stderr, "\n Error: error getting all the input from the user, please try again. \n");
		}while(txt_len == 0);

		size_t tmp2 = strlen(buff);
		
		if(strcmp(buff, "bye") == 0){
			mssg = createPacketFromData(buff, tmp2, opcode);
			mssg->opcode = '3';
			sendStructAsCharArr(*sockfd, mssg);
			fprintf(stdout, "Exiting\n");
			return 0;
		}
		else{
			mssg = createPacketFromData(buff, tmp2, opcode);
			mssg->opcode = '2';
		}
		
		//Check if the packet is under the max size
		if(mssg->mssg_len <= MAX_BUFFER_SIZE){
			//Send the packet
			sendStructAsCharArr(*sockfd, mssg);
		}
		else{
			
		}
		mssg->text[mssg->mssg_len-3] = (char) '\0';
		fprintf(stdout, "\nYou:> %s\n", mssg->text);
		
		free(mssg);
		free(buff);
	}
	return 0;
}

/**This method sends the char array version of the struct mssg_struct**/
void sendStructAsCharArr(int sockfd, struct mssg_struct* mssg){	
	char *struct_arr;
	int array_len = 0;
	struct_arr = convertMssgStructToArray(mssg, struct_arr);
	
	unsigned short struct_arr_len = strlen(struct_arr);
	
	//fprintf(stdout, "This is the converted txt %s\n", struct_arr);
	
	pthread_mutex_trylock(&mutex);
	send(sockfd, struct_arr, struct_arr_len, 0);
	pthread_mutex_unlock(&mutex);
	
	free(struct_arr);
}

/**Set the timeout for the given socket file descriptor*/
bool setSocketTimeOut(int sockfd){
	struct timeval timeout;
	timeout.tv_sec = 30;
	timeout.tv_usec = 0;
	
	if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0){
		fprintf(stderr, "Error: setsockopt failed\n");
		return false;
	}
	
	if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0){
		fprintf(stderr, "Error: setsockopt failed\n");
		return false; 
	}
	
	return true;
}

/**This method is used to set up the struct variable
 * of type sockadd_in for the information about the server*/
bool setUpServerInfo(struct sockaddr_in* serv_addr, struct arg_struct* args){
	
	serv_addr->sin_family = AF_INET;
	serv_addr->sin_port = htons(args->port);
	
	if(inet_pton(AF_INET, args->ip_domain_name, &serv_addr->sin_addr) <= 0){
		//if the ip address didn't work, try converting the given value
		//	as a domain name
		//fprintf(stdout, "The address given isn't an IP address.\n");
		struct hostent* tmp = gethostbyname(args->ip_domain_name);
		struct in_addr **addr_list;
		addr_list = (struct in_addr **)tmp->h_addr_list;
		char* ip_addr = inet_ntoa(*addr_list[0]);
		//fprintf(stdout, "This is the ip address of the given domain name: %s\n", ip_addr);
		
		if(inet_pton(AF_INET, ip_addr, &serv_addr->sin_addr) <= 0){
			fprintf(stderr, "\n Error: Invalid address.\n");
			return false;
		}
		else
			return true;		
	}
	else{
		return true;
	}
	
	return false;
}

/**Start chatting with the other user*/
void startChatting(int sockfd, char* name){
	struct mssg_struct* mssg;
	char op = ' ';
	
	fprintf(stdout, "---Starting typing and sending mssgs---\n");
	
	//Set up the thread attributes
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	
	pthread_t threadID;
	//This thread will be doing the writing part
	if(pthread_create(&threadID, &attr, sendChatMessages, &sockfd) < 0){
		fprintf(stderr, "\n Error: Couldn't create thread.\n");
	}
	
	//This main thread loop will be doing the listening part 
	while(opcode != '3'){
		//First recieve will be the name	
		pthread_mutex_trylock(&mutex);
		mssg = receiveStructArray(sockfd);
		pthread_mutex_unlock(&mutex);
		
		op = mssg->opcode;
		
		pthread_mutex_trylock(&mutex);
		opcode = op;
		pthread_mutex_unlock(&mutex);

		if(mssg != NULL){
			//fprintf(stdout, "Recieved something: %s \n", mssg->text);
			mssg->text[mssg->mssg_len - 3] = (char) '\0';
			if(opcode == '2'){
				fprintf(stdout, "\n%s:> %s\n\n", name, mssg->text);
			}
			else if(opcode == '3'){
				fprintf(stdout, "Received the exitin mssg.\n");
				free(mssg);
				free(name);
				return;
			}
		}
		free(mssg);
		sleep(1);
		mssg = NULL;
	}
	free(name);
}

