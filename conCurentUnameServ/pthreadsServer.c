/*
 * References
 * - https://www.geeksforgeeks.org/udp-server-client-implementation-c/
 * - http://www.microhowto.info/
 * - Class slides - 05concurr_server1.ppt
 * - https://stackoverflow.com/questions/4046616/sockets-how-to-find-out-what-port-and-address-im-assigned
 * */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

void *handleTCPConnections(void*);
void *handleUDPConnections(void*);
char* processString(char*);
void reaper(int);
char* removeSpaceAndEndOfLineChar(char*);

//global variables
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//structure used for pthread_create arg passing
struct arg_struct{
	int sockfd;
	char recvBuf[20];
	struct sockaddr_in cliaddr;
};




int main(int argc, char* argv[]){
	
	bool portNumGiven = true;
	bool isTcp = true;
	
	if(argc > 4 || argc < 2){
		fprintf(stderr, "Incorrect number of arguments.\n");
		fprintf(stderr, "\tArguements passed should be in the following format: \n\tunameserver (-tcp|-udp) [-port PortNumber]\n");
		return -1;
	}
	//If no -port and no portnumber  
	else if((argc == 3 && strncmp(argv[argc - 1], "-port", 4) == 0) || argc == 2)
		portNumGiven = false;
	
	char* protocol = "";
	int sock_type = 0;
	int port_num = 0;
	
	//TCP or UDP?
	if(strncmp(argv[1], "-tcp", 4) == 0){
		protocol = "tcp";
		sock_type = SOCK_STREAM;
	}
	else if(strncmp(argv[1], "-udp", 4) == 0){
		protocol = "udp";
		sock_type = SOCK_DGRAM;
		isTcp = false;
	}
		
	//Create a socket
	//keeping it ipv4 for now
	int addr_type = AF_INET;
	int sockfd = socket(addr_type, sock_type, 0);
	
	if(sockfd < 0){
		fprintf(stderr, "Couldn't create the socket\n");
		return -1;
	}
	
	struct sockaddr_in addrport;
	
	//If the user gave the port number through command line arguements
	if(portNumGiven){
		char* temp = argv[argc-1];
		port_num = atoi(argv[argc-1]);
		fprintf(stdout, "Given portnumber %i\n", port_num);
		addrport.sin_family = addr_type;
		addrport.sin_port = htons(port_num);
		addrport.sin_addr.s_addr = INADDR_ANY;
	}
	else{
		addrport.sin_family = addr_type;
		addrport.sin_addr.s_addr = INADDR_ANY;
	}
	
	int addrPorLen = sizeof(addrport);
	
	//Do the binding
	if(bind(sockfd, (struct sockaddr *) &addrport, sizeof(addrport)) < 0){
		fprintf(stderr, "\tError binding.\n");
		return -1;
	}
	
	//Listening
	int queueLimit = 3;
	if(isTcp)
		if(listen(sockfd, queueLimit) < 0){
			fprintf(stderr, "\nFailed to listen.\n");
			return -1;
	}
	
	//Get the assigned port number
	if(!portNumGiven){
		socklen_t len = sizeof(addrport);
		if(getsockname(sockfd, (struct sockaddr *)&addrport, 
			&len) != -1){
			port_num = ntohs(addrport.sin_port);	
			fprintf(stdout, "\nListening on port number %i\n", port_num);
		}
		else
			fprintf(stderr, "\nError: %s\n", strerror(errno));
	}
	else{
		fprintf(stdout, "\nThis is the portnumber you gave: %i\n", port_num);
	}
	
	//Accept
	int status = 0;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	while(true){
		//fprintf(stdout, "\nListening on port number %i\n", port_num);
		if(isTcp){
			pthread_t threadID;
			status = accept(sockfd, (struct sockaddr *)&addrport, (socklen_t*)&addrPorLen);
			if(pthread_create(&threadID, &attr, handleTCPConnections, 
			&status) < 0){
				fprintf(stderr, "\nFailed to create a thread: %s\n", 
				strerror(errno));
				return -1;
			}
			//handleTCPConnections(status);	//This accept socket is a copy which won't be used by the parent, so we can just close it
		}
		else{
			struct sockaddr_in cliaddr;
			//char* recvBuf = malloc(len * sizeof(char));
			memset(&cliaddr, 0, sizeof(cliaddr));
			int len = 32;	
			char recvBuf[20] = {'\0'}; 
			socklen_t lenOfCli = sizeof(cliaddr);
			pthread_mutex_trylock(&mutex);
			int count = recvfrom(sockfd, (char*)recvBuf, len, 0, (struct sockaddr *)&cliaddr, &lenOfCli);
			pthread_mutex_unlock(&mutex);
			if(count > 0){
				struct arg_struct args;
				pthread_t threadID;
				//initialize the arg struct
				args.sockfd = sockfd;
				strcpy(args.recvBuf, recvBuf);
				args.cliaddr = cliaddr;
				
				if(pthread_create(&threadID, &attr, handleUDPConnections, 
				(void*) &args) < 0){
					fprintf(stderr, "\nFailed to create a thread: %s\n", 
					strerror(errno));
					return -1;
				}
			}
		}
	}
	close(sockfd);
	
	return 0; 
}

//This method handles the TCP connections
void *handleTCPConnections(void *s){
	int *status = (int *)s;
	if(status < 0){
		fprintf(stderr, "\nFailed to accept.\n");
		return NULL;
	}
	fprintf(stdout, "\nSocket file descriptor %i\n", *status);
	struct timeval timeout;
	timeout.tv_sec = 5;
	timeout.tv_usec = 0;
	
	if(setsockopt(*status, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
		fprintf(stderr, "Error: setsockopt failed\n");
	
	if(setsockopt(*status, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0)
		fprintf(stderr, "Error: setsockopt failed\n");
	
	//recieve
	int len = 32;
	//char* recvBuf = malloc(len * sizeof(char));
	//memset(recvBuf, '\0', sizeof(len));
	char recvBuf[20] = {'\0'}; 
	int count = recv(*status, (char*)&recvBuf, len, 0);
	char* result;
	fprintf(stdout, "Recieved: %s\n", recvBuf);
	//free(recvBuf);
	result = processString(recvBuf);
	//fprintf(stdout, "String to send: %s\n", result);
	
	count = send(*status, result, strlen(result), 0);
	//fprintf(stdout, "\nString to send to: %s\n", inet_ntoa(addrport.sin_addr));
	close(*status);//After done with the socket, close it
	return 0;
}

//This method handles udp connections
void *handleUDPConnections(void *a){
	struct arg_struct *args = a;
	char* result;
	fprintf(stdout, "Recieved: %s\n", args->recvBuf);
	//free(recvBuf);
	result = processString(args->recvBuf);
	socklen_t lenOfCli = sizeof(args->cliaddr);
	pthread_mutex_trylock(&mutex);
	sendto(args->sockfd, (const char *)result, strlen(result), 0, (const 
	struct sockaddr *)&args->cliaddr, lenOfCli);
	pthread_mutex_unlock(&mutex);
		
	return 0;
}

//This method works with the string recieved from the client
char* processString(char* recvdStr){
	FILE* fp;
	fprintf(stdout, "String to process: %s\n", recvdStr);
	char path[1024];
	char *uname = "uname -";
	char result[32];
	//char tmp[32];
	char* recvdStr2 = removeSpaceAndEndOfLineChar(recvdStr);
	strcpy(result, uname);
	strcat(result, recvdStr2);
	
	printf("%s\n", result);
	
	
	fp = popen((char*)&result, "r");
	
	if(fp == NULL){
		int tmp = pclose(fp);
		fprintf(stderr, "\tError: while trying to do popen: %s\n", strerror(tmp));
		return NULL;
	}
	else{
		while(fgets((path), sizeof(path), fp) != NULL){
			fprintf(stdout, "\n%s\n", path);	
		}
	}
	
	pclose(fp);
	
	char *ret = path;
	return ret;
}

char* removeSpaceAndEndOfLineChar(char* recvdStr){
	int len = strlen(recvdStr);
	char* strPtr;
	strPtr = recvdStr;
	int i = 0;
	int j = 0;
	while(recvdStr[j] != '\0' && j < len){
		if(*strPtr == '\r' || *strPtr == '\n'){
			//Find a char that isn't a space or end of line
			while((recvdStr[j] == '\r' || recvdStr[j] == '\n') && j < len)
				j++;
			*strPtr = recvdStr[j];
			j++;
		}
		else
			j++;
		strPtr++;
	}
	*strPtr = '\0';
	
	printf("%s\n", recvdStr);
	return recvdStr;
}
